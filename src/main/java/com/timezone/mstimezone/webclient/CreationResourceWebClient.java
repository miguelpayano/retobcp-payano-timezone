package com.timezone.mstimezone.webclient;

import com.timezone.mstimezone.expose.response.changetype.ChangeTypeApiResponse;
import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import org.springframework.http.ResponseEntity;

public interface CreationResourceWebClient {


    RespuestaTimeZone getTimeZoneClient(String zone) throws Exception;

    String getChangeTypeWebClient(String moneyOne, String moneTwo) throws Exception;


}
