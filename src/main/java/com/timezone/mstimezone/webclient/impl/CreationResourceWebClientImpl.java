package com.timezone.mstimezone.webclient.impl;

import com.timezone.mstimezone.expose.exception.BusinessExepcion;
import com.timezone.mstimezone.expose.response.changetype.ChangeTypeApiResponse;
import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.webclient.CreationResourceWebClient;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CreationResourceWebClientImpl  implements CreationResourceWebClient {

    @Autowired
    RestTemplate restTemplate;

    Gson gson = new Gson();

    @Override
    public RespuestaTimeZone getTimeZoneClient(String zone) throws Exception {
        RespuestaTimeZone respuestaTimeZone=null;
        try {
             respuestaTimeZone = restTemplate.getForObject(
                    "https://worldtimeapi.org/api/timezone/America/" + zone,
                    RespuestaTimeZone.class);

        } catch (Exception e) {
            throw  new BusinessExepcion(
                    "00601",
                    HttpStatus.NOT_FOUND,
                    "BCP:Error al consultar Api externo"
                            + "https://worldtimeapi.org/api/timezone/America/" +  zone);
        }

//        WebClient api = WebClient.create("https://worldtimeapi.org/api/timezone/America");
//        Mono<RespuestaTimeZone> respuestaTimeZoneMono;
//
//        respuestaTimeZoneMono =   api.get()
//                .uri("/Lima")
//                .retrieve()
//                .bodyToMono(RespuestaTimeZone.class)
//                .onErrorResume(WebClientResponseException.class,
//                        ex -> ex.getRawStatusCode() == 404 ? Mono.empty() : Mono.error(ex));
//
//        log.info("respuesta " +  respuestaTimeZoneMono.toFuture().get());

        return gson.fromJson(
                gson.toJson(respuestaTimeZone), RespuestaTimeZone.class);

    }

    @Override
    public String getChangeTypeWebClient(String moneyOne, String moneTwo) throws Exception {
        ChangeTypeApiResponse changeTypeApiResponse=null;
        log.info("variables : moneyOne:"+moneyOne+" moneTwo="+moneTwo);
        String response;

        try {

            RestTemplate restTemplate = new RestTemplate();
            String fooResourceUrl
                    = "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
                            + moneyOne+"/"+moneTwo+".json";
            response = restTemplate.getForObject(fooResourceUrl, String.class);

            log.info("respuesta :" + response);

//            changeTypeApiResponse = restTemplate.getForObject(
//                    "https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
//                            + moneyOne+"/"+moneTwo+".json",
//                    ChangeTypeApiResponse.class);

        } catch (Exception e) {
            throw  new BusinessExepcion(
                    "00601",
                    HttpStatus.NOT_FOUND,
                    "BCP:Error al consultar Api externo"
                            +"https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
                            + moneyOne.toLowerCase()+"/"+moneTwo.toLowerCase()+".json"
                    );
        }

        return response;
    }


}
