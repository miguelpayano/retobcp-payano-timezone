package com.timezone.mstimezone.repository;

import com.timezone.mstimezone.model.TimeZoneEntity;
import com.timezone.mstimezone.model.VariablesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariablesRepository extends JpaRepository<VariablesEntity,Integer> {

    @Query("SELECT e FROM VariablesEntity e where e.name=:name")
    List<VariablesEntity> getByNameVariables(
            @Param("name")String name
    );

    @Query("SELECT e FROM VariablesEntity e")
    List<VariablesEntity> getAllVariables();
}
