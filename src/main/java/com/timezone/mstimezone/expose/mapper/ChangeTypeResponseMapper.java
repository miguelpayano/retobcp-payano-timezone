package com.timezone.mstimezone.expose.mapper;

import com.timezone.mstimezone.expose.response.changetype.ChangeTypeResponse;
import com.timezone.mstimezone.model.ChangeTypeEntity;
import org.springframework.stereotype.Component;

@Component
public class ChangeTypeResponseMapper {

    public final static ChangeTypeResponse buildChangeType(ChangeTypeEntity changeTypeEntity){
        ChangeTypeResponse changeTypeResponse = new ChangeTypeResponse();
        changeTypeResponse.setId(changeTypeEntity.getId());
        changeTypeResponse.setDescription(changeTypeEntity.getDescription());
        changeTypeResponse.setMoneyOne(changeTypeEntity.getMoneyOne());
        changeTypeResponse.setMoneyTwo(changeTypeEntity.getMoneyTwo());
        changeTypeResponse.setValue(changeTypeEntity.getValue());
        changeTypeResponse.setDate(changeTypeEntity.getDate());
        return changeTypeResponse;
    }

}
