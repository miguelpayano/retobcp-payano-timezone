package com.timezone.mstimezone.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.timezone.mstimezone.expose.exception.BusinessExepcion;
import com.timezone.mstimezone.expose.mapper.ChangeTypeResponseMapper;
import com.timezone.mstimezone.expose.response.changetype.ChangeTypeApiResponse;
import com.timezone.mstimezone.expose.response.changetype.ChangeTypeResponse;
import com.timezone.mstimezone.model.ChangeTypeEntity;
import com.timezone.mstimezone.model.TimeZoneEntity;
import com.timezone.mstimezone.model.VariablesEntity;
import com.timezone.mstimezone.repository.ChangeTypeRepository;
import com.timezone.mstimezone.service.ChangeTypeService;
import com.timezone.mstimezone.webclient.CreationResourceWebClient;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class ChangeTypeServiceImpl implements ChangeTypeService {

    @Autowired
    private CreationResourceWebClient createResourceWebClient;


    @Autowired
    private ChangeTypeRepository changeTypeRepository;

    @Override
    public Observable<ChangeTypeResponse> getAllTypeResponse() throws Exception {
        List<ChangeTypeEntity> changeTypeEntityList = new ArrayList<>();
        changeTypeEntityList = changeTypeRepository.getAllChangeType();
        return Observable.just(changeTypeEntityList).flatMapIterable(x -> x)
                .map(ChangeTypeResponseMapper::buildChangeType);
    }



    @Override
    public Single<ChangeTypeResponse> getByChangeTypeApi(
            String moneyOne, String moneTwo) throws Exception {
        Gson gson = new Gson();

        ChangeTypeEntity changeTypeEntity;
        ChangeTypeResponse changeTypeResponse = null;
        ChangeTypeApiResponse changeTypeApiResponse = null;

        try {
            Optional<ChangeTypeEntity> optChangeType = changeTypeRepository.getchangeTypeByMoneyOneAndMoneyTwo(
                    moneyOne.toUpperCase(),
                    moneTwo.toUpperCase());

            if(!optChangeType.isPresent()){
                log.info("iniciamos llamada al api publica : {} " );
                log.info("https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/"
                        +  moneyOne.toUpperCase() + "/"+ moneTwo.toUpperCase()+ ".json" );

                String response;

                response = createResourceWebClient.getChangeTypeWebClient(moneyOne,moneTwo);

                JSONObject jsonObject = new JSONObject(response);
                log.info("jsonObject " + jsonObject );

                String cadena = jsonObject.toString();
                String sinCorchete = cadena.replace("{", "").replace("}","");
                log.info(sinCorchete);

                String date =  sinCorchete.substring(8,18);
                String stringValor[] =  sinCorchete.split(",");
                String monedaValor =  stringValor[stringValor.length -1];

                String valor3[] =  monedaValor.split(":");
                String valor4 = valor3[valor3.length - 1];

                log.info("date " + date);
                log.info("valor " + valor4);

                log.info("iniciamos el grabado en la base");

                ChangeTypeEntity changeTypeEntity1 =  new ChangeTypeEntity();

                changeTypeEntity1.setDescription("Convertir de " + moneyOne.toUpperCase() + "a " + moneTwo.toUpperCase());
                changeTypeEntity1.setMoneyOne(moneyOne.toUpperCase());
                changeTypeEntity1.setMoneyTwo(moneTwo.toUpperCase());
                changeTypeEntity1.setDate(date);
                changeTypeEntity1.setValue(new BigDecimal(valor4));
                changeTypeRepository.save(changeTypeEntity1);

                changeTypeResponse = gson.fromJson(
                        gson.toJson(changeTypeEntity1), ChangeTypeResponse.class);

            }else {
                changeTypeResponse = gson.fromJson(
                        gson.toJson(optChangeType.get()), ChangeTypeResponse.class);
            }

        }catch (Exception e){
            throw  new BusinessExepcion(
                    "0545",
                    HttpStatus.BAD_REQUEST,
                    "BCP:Error al crear tipo de cambio verificar bien la meneda enviada");
        }



        return Single.just(changeTypeResponse);


    }
}
