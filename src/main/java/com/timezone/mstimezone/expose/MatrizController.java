package com.timezone.mstimezone.expose;

import com.timezone.mstimezone.expose.response.variables.VariablesResponse;
import com.timezone.mstimezone.service.TimeZoneService;
import io.reactivex.rxjava3.core.Observable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/matriz")
@CrossOrigin
@Slf4j
public class MatrizController {
    @Autowired
    private TimeZoneService timeZoneService;


    @GetMapping(value="")
    public void getAllByNameVariable(
            @RequestParam Integer userValue)
            throws Exception {
        log.info("inicio de llamada a servicio que genera la matriz");
         timeZoneService.getMatriz(userValue);
    }

}
