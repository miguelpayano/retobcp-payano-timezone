package com.timezone.mstimezone.expose;

import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.expose.response.variables.VariablesResponse;
import com.timezone.mstimezone.service.TimeZoneService;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/variables")
@CrossOrigin
@Slf4j
public class VariablesController {

    @Autowired
    private TimeZoneService timeZoneService;


    @GetMapping(value="")
    public Observable<VariablesResponse> getAllByNameVariable(
            @RequestParam String name)
            throws Exception {
    log.info("inicio de llamada a servicio getTimeZone");
        return timeZoneService.getAllByNameVariables(name);
    }
}
