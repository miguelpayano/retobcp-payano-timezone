package com.timezone.mstimezone.expose.mapper;

import com.timezone.mstimezone.expose.response.variables.VariablesResponse;
import com.timezone.mstimezone.model.VariablesEntity;
import org.springframework.stereotype.Component;

@Component
public class VariablesResponseMapper {
    public final static VariablesResponse buildVariables(VariablesEntity variablesEntity){
        VariablesResponse variablesResponse = new VariablesResponse();
            variablesResponse.setId_variable(variablesEntity.getId_variable());
            variablesResponse.setName(variablesEntity.getName());
            variablesResponse.setValue(variablesEntity.getValue());
        return variablesResponse;
    }
}
