package com.timezone.mstimezone.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
// payano
@Getter
@Setter
@Entity
@Table(name = "time_zone")
public class TimeZoneEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_time_zone")
    private Integer idTimeZone;

    @Column(name = "abbreviation")
    private String abbreviation;

    @Column(name = "client_ip")
    private String client_ip;

    @Column(name = "datetime")
    private String datetime;

    @Column(name = "day_of_week")
    private Integer day_of_week;

    @Column(name = "day_of_year")
    private Integer day_of_year;

    @Column(name = "dst")
    private String dst;

    @Column(name = "dst_from")
    private String dst_from;

    @Column(name = "dst_offset")
    private Integer dst_offset;

    @Column(name = "dst_until")
    private String dst_until;

    @Column(name = "raw_offset")
    private String raw_offset;

    @Column(name = "timezone")
    private String timezone;

    @Column(name = "unixtime")
    private String unixtime;

    @Column(name = "utc_datetime")
    private String utc_datetime;

    @Column(name = "utc_offset")
    private String utc_offset;

    @Column(name = "week_number")
    private Integer week_number;

}
