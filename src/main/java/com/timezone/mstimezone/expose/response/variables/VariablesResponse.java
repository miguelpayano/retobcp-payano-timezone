package com.timezone.mstimezone.expose.response.variables;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class VariablesResponse {
    private Integer id_variable;
    private String name;
    private String value;
}
