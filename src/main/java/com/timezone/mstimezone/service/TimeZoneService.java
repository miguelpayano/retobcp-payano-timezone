package com.timezone.mstimezone.service;

import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.expose.response.variables.VariablesResponse;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;


public interface TimeZoneService {


    Observable<TimeZoneResponse> getAllTimeZone(String name) throws Exception;

    Single<RespuestaTimeZone> getByZoneTimeZone(String zone) throws  Exception;


    Observable<VariablesResponse> getAllByNameVariables(String name) throws Exception;

    void getMatriz(Integer userValue) throws Exception;


}
