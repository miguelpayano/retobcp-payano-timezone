package com.timezone.mstimezone.expose.exception;


import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class BusinessExepcion extends RuntimeException {

    private String code;
    private HttpStatus status;

    public BusinessExepcion(String code, HttpStatus status, String message) {
        super(message);
        this.code = code;
        this.status = status;
    }
}
