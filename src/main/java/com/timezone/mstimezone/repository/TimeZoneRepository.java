package com.timezone.mstimezone.repository;

import com.timezone.mstimezone.model.TimeZoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TimeZoneRepository extends JpaRepository<TimeZoneEntity,Integer> {


    @Query("SELECT e FROM TimeZoneEntity e where e.timezone=:name")
    List<TimeZoneEntity> getByNameTimeZone(
            @Param("name")String name
    );

    @Query("SELECT e FROM TimeZoneEntity e")
    List<TimeZoneEntity> getAllTimeZone();

    @Query(value = "SELECT * FROM time_zone e where e.timezone=:timezone",nativeQuery = true)
    Optional<TimeZoneEntity> findByTimeZone(
            @Param("timezone")String timezone);



}
