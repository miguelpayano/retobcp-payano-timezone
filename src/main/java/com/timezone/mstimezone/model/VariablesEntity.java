package com.timezone.mstimezone.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "table_variables")
public class VariablesEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_variable")
    private Integer id_variable;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;
}
