package com.timezone.mstimezone.expose.response.changetype;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ChangeTypeResponse {
    private Integer id;
    private String description;
    private String moneyOne;
    private String moneyTwo;
    private BigDecimal value;
    private String date;

}
