package com.timezone.mstimezone.service.impl;

import com.timezone.mstimezone.expose.exception.BusinessExepcion;
import com.timezone.mstimezone.expose.mapper.TimeZoneResponseMapper;
import com.timezone.mstimezone.expose.mapper.VariablesResponseMapper;
import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.expose.response.variables.VariablesResponse;
import com.timezone.mstimezone.model.TimeZoneEntity;
import com.timezone.mstimezone.model.VariablesEntity;
import com.timezone.mstimezone.repository.TimeZoneRepository;
import com.timezone.mstimezone.repository.VariablesRepository;
import com.timezone.mstimezone.service.TimeZoneService;
import com.timezone.mstimezone.webclient.CreationResourceWebClient;
import com.timezone.mstimezone.webclient.model.RespuestaTimeZone;
import com.google.gson.Gson;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TimeZoneServiceImpl implements TimeZoneService {

    private final CreationResourceWebClient createResourceWebClient;

    @Autowired
    private TimeZoneRepository timeZoneRepository;

    @Autowired
    private VariablesRepository variablesRepository;


    @Override
    public Observable<TimeZoneResponse> getAllTimeZone(String name) throws Exception {

        List<TimeZoneEntity> timeZoneList = new ArrayList<>();

        if(name.isEmpty()){
            timeZoneList = timeZoneRepository.getAllTimeZone();
        }else if(!name.isEmpty()) {
            timeZoneList = timeZoneRepository.getByNameTimeZone("America/"+name);
            if(timeZoneList.size()==0){
                //timeZoneList = timeZoneRepository.getAllTimeZone();
                throw  new BusinessExepcion(
                        "00666",
                        HttpStatus.NOT_FOUND,
                        "BCP: No existe el recurso consultado, verificar bien el parametro de envio..");            }
        }

        return Observable.just(timeZoneList).flatMapIterable(x -> x)
                .map(TimeZoneResponseMapper::buildTimeZone);
    }

    @Override
    public Single<RespuestaTimeZone> getByZoneTimeZone(String zone) throws Exception {
        Gson gson = new Gson();
        long inicio;
        long inicioE = 0L;

        TimeZoneEntity timeZoneEntity = null;
        Optional<TimeZoneEntity> timeZoneEntityOptional=null;

        RespuestaTimeZone respuestaTimeZone = null;
        timeZoneEntityOptional = timeZoneRepository.findByTimeZone("America/"+zone);

        VariablesResponse variablesResponse=null;

        try {
            if(!timeZoneEntityOptional.isPresent()){
                respuestaTimeZone = createResourceWebClient.getTimeZoneClient(zone);

                timeZoneEntity = gson.fromJson(
                        gson.toJson(respuestaTimeZone), TimeZoneEntity.class);
                if(respuestaTimeZone!=null){
                    log.info("gresamos despues de hacer la peticion  e iniciamos el grabado en la base");
                    timeZoneRepository.save(timeZoneEntity);

                    VariablesEntity variablesEntity = new VariablesEntity();
                    String timezone[] = timeZoneEntity.getTimezone().split("/");
                    variablesEntity.setName(timezone[timezone.length - 1]);
                    variablesEntity.setValue(timeZoneEntity.getUtc_offset());

                    variablesRepository.save(variablesEntity);
                }
            }else {
                respuestaTimeZone = gson.fromJson(
                        gson.toJson(timeZoneEntityOptional.get()), RespuestaTimeZone.class);
            }

        } catch (Exception ex) {
            log.info("Servicio apagado ");
            long fin = System.currentTimeMillis();
            double tiempo = (double) fin - inicioE;
            log.info("[FIN con Error crear Time Zone] error: " + ex.getLocalizedMessage() + ","
                    + " Tiempo total del proceso de ejecución: "
                    + tiempo + " ms");
            throw  new BusinessExepcion(
                    "00777",
                    HttpStatus.BAD_REQUEST,
                    "BCP:Error al crear timezone");
        }

        log.info("respuesta de web clinet : " + respuestaTimeZone);

        return Single.just(respuestaTimeZone);
    }

    @Override
    public Observable<VariablesResponse> getAllByNameVariables(String name) throws Exception {
        List<VariablesEntity> variablesEntityList = new ArrayList<>();
        variablesEntityList = variablesRepository.getByNameVariables("America/"+name);
        if(name.isEmpty()){
            variablesEntityList = variablesRepository.getAllVariables();
        }else if(!name.isEmpty()) {
            variablesEntityList =  variablesRepository.getByNameVariables(name);;
            if(variablesEntityList.size()==0){
                //variablesEntityList = variablesRepository.getAllVariables();
                throw  new BusinessExepcion(
                        "00777",
                        HttpStatus.NOT_FOUND,
                        "BCP: No existe el recurso consultado, verificar bien el parametro de envio..");            }
        }

        return Observable.just(variablesEntityList).flatMapIterable(x -> x)
                .map(VariablesResponseMapper::buildVariables);
    }

    @Override
    public void getMatriz(Integer userValue) throws Exception {
        //int userValue = 2;


        int [][] ventas = new int[10][10];

        ventas[0][0] = 10;
        StringBuilder pintado = new StringBuilder();
        pintado.append(" \n");
        System.out.print("valor de Input: userValue= " + userValue  + "\n");
        //log.info("valor Input: userValue= " + userValue  + "\n");

        // Inicio el recorrido por filas y columnas.
        for (int i = 0; i < ventas.length; i++) {
            // Hacemos el recorrido de filas i
            pintado.append("[");
            for (int j = 0; j < ventas[i].length; j++) {
                // Hacemos el recorrido de la columna j
                ventas[i][j] = (userValue * i) + (i == 0 ? 0 : ventas[i - 1][j]);
                pintado.append(ventas[i][j] + ",");
                //log.info("A[" + i + "][" + j + "]= " + ventas[i][j] + "\n");

            }
            pintado.append("] \n");
        }
        log.info("" + pintado);
        //System.out.print(pintado);
        pintado.append(" \n");
    }

    public static boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }



}
