package com.timezone.mstimezone.expose.mapper;

import com.timezone.mstimezone.expose.response.timezone.TimeZoneResponse;
import com.timezone.mstimezone.model.TimeZoneEntity;
import org.springframework.stereotype.Component;

@Component
public class TimeZoneResponseMapper {
    public final static TimeZoneResponse buildTimeZone(TimeZoneEntity timeZoneEntity){
        TimeZoneResponse timeZoneResponse = new TimeZoneResponse();
        timeZoneResponse.setIdTimeZone(timeZoneEntity.getIdTimeZone());
        timeZoneResponse.setAbbreviation(timeZoneEntity.getAbbreviation());
        timeZoneResponse.setClient_ip(timeZoneEntity.getClient_ip());
        timeZoneResponse.setDatetime(timeZoneEntity.getDatetime());
        timeZoneResponse.setDay_of_week(timeZoneEntity.getDay_of_week());
        timeZoneResponse.setDay_of_year(timeZoneEntity.getDay_of_year());
        timeZoneResponse.setDst(timeZoneEntity.getDst());
        timeZoneResponse.setDst_from(timeZoneEntity.getDst_from());
        timeZoneResponse.setDst_offset(timeZoneEntity.getDst_offset());
        timeZoneResponse.setDst_until(timeZoneEntity.getDst_until());
        timeZoneResponse.setRaw_offset(timeZoneEntity.getRaw_offset());
        timeZoneResponse.setTimezone(timeZoneEntity.getTimezone());
        timeZoneResponse.setUnixtime(timeZoneEntity.getUnixtime());
        timeZoneResponse.setUtc_datetime(timeZoneEntity.getUtc_datetime());
        timeZoneResponse.setUtc_offset(timeZoneEntity.getUtc_offset());
        timeZoneResponse.setWeek_number(timeZoneEntity.getWeek_number());
        return timeZoneResponse;
    }

}
