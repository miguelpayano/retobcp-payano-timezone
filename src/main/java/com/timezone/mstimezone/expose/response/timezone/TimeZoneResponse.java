package com.timezone.mstimezone.expose.response.timezone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimeZoneResponse {
    private Integer idTimeZone;
    private String abbreviation;
    private String client_ip;
    private String datetime;
    private Integer day_of_week;
    private Integer day_of_year;
    private String dst;
    private String dst_from;
    private Integer dst_offset;
    private String dst_until;
    private String raw_offset;
    private String timezone;
    private String unixtime;
    private String utc_datetime;
    private String utc_offset;
    private Integer week_number;
}
